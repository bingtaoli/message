#include "sc.h"

void sc_log(const char *format, ...)
{
    FILE *fp = stdout;
    fprintf(fp, ANSI_FG_GREEN);
    va_list args;
    va_start(args, format);
    vfprintf(fp, format, args);
    va_end(args);
    fprintf(fp, ANSI_RESET);
}

void sc_error(const char *format, ...)
{
    fprintf(stderr, ANSI_FG_RED);
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fprintf(stderr, ANSI_RESET "\n");
}

void sc_warn(const char *format, ...)
{
    FILE *fp = stderr;
    fprintf(fp, ANSI_FG_YELLOW);
    va_list args;
    va_start(args, format);
    vfprintf(fp, format, args);
    va_end(args);
    fprintf(fp, ANSI_RESET "\n");
}


uint8_t align(uint8_t size){
    // while (size%4 != 0){
    //     size++;
    // }
    return size;
}

uint8_t hex_to_dec(char *s)
{
    int i,t;
    uint8_t sum=0;
    for(i=0; s[i]; i++)
    {
        if(s[i] <= '9'){
            t=s[i]-'0';
        } else{
            t = s[i] - 'a' + 10;
        }
        sum = sum*16 + t;
    }
    return sum;
}

// hex to dec
uint8_t wtoi(char *hexa){
    if (strlen(hexa) != 2){
        printf("ERROR >> format error ! please ensure parameter's length is 2.\n");
        exit(EXIT_FAILURE);
    }
    uint8_t temp_uint = hex_to_dec(hexa);
    return temp_uint;
}

/*
@str_mac: eg 88:88:88:88:88:88
@dst_mac: uint8_t dst_mac[6]
*/
void set_mac(OUT uint8_t dst_mac[6], IN char *str_mac){
    const char *spliter = ":";
    char *sp = strtok(str_mac, spliter);
    while (sp != NULL){
        *dst_mac = wtoi(sp);
        dst_mac++;
        sp = strtok(NULL, spliter);
    }
}

void output_mac(uint8_t *mac){
    int i;
    for (i=0; i<5; i++) {
        printf ("%02x:", mac[i]);
    }
    printf ("%02x\n", mac[5]);
}

void format_output_student(studentInfo *pstudent){
    printf("%s | %s | %s\n", pstudent->szStudentSchool, pstudent->szStudent, pstudent->szStudentName);
}

//检测网卡是否有效
int test_interface(char *interface){
    int sd;
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(struct ifreq));

    //第一次创建socket是为了获取本地网卡信息
    if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_IP))) < 0) {
        sc_error ("socket() failed to get socket descriptor for using ioctl() ");
        exit (EXIT_FAILURE);
    }
    // Use ioctl() to look up interface name and get its MAC address.
    memset (&ifr, 0, sizeof (ifr));
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", interface);
    if (ioctl (sd, SIOCGIFHWADDR, &ifr) < 0) {
        sc_error ("ioctl() failed to get source MAC address ");
        return EXIT_FAILURE;
    }
    close (sd);
    return EXIT_SUCCESS;
}

void add_tlv(INOUT Tlv **buf_pointer, IN const uint8_t type, IN const uint8_t len, IN const char *data){
    (*buf_pointer)->type = type;
    (*buf_pointer)->len = len;
    strncpy((*buf_pointer)->data, data, len);
    ((*buf_pointer)->data)[len] = '\0';

    // pointer tranform
    char *buf_pointer_tmep = (char *)(*buf_pointer);
    buf_pointer_tmep += align((sizeof(Tlv)) + len + 1);
    (*buf_pointer) = (Tlv *)buf_pointer_tmep;
}

//是否是我们需要的协议
bool is_our_protocol(char *buf){
    // test whether protocol is 0X1122
    int index;
    uint8_t protocol[2];
    for (index=12; index<14; index++){
        protocol[index-12] = buf[index];
    }
    char protocol_str[5];
    snprintf(protocol_str, 5, "%02x%02x", protocol[0], protocol[1]);
    if (strncmp(protocol_str, "1122", 4) == 0){
        return true;
    }
    return false;
}

//打印报文信息
void output_packet(uint8_t *ether_frame){
    char *tlv_temp = NULL;
    int length = 0;

    uint8_t test_src_mac[6];
    uint8_t test_dst_mac[6];

    memcpy(test_dst_mac, ether_frame, 6);
    memcpy(test_src_mac, ether_frame+6, 6);
    printf("srcMac: ");
    output_mac(test_src_mac);
    printf("dstMac: ");
    output_mac(test_dst_mac);

    Control *control_field = (Control *)(ether_frame + 14);
    // printf("is_start: %d\n", control_field->start);
    // printf("is_end: %d\n", control_field->end);
    printf("fragment_number: %d\n", control_field->fragment_number);
    printf("msg_type: %d\n", control_field->type);

    // 把tlv都打印出来
    Tlv *tlv = (Tlv *)(void *)(ether_frame+22);
    while (tlv->type != 0){
        length = tlv->len;
        printf("data: %s\tlength: %d\n", tlv->data, length);
        tlv_temp = (char *)tlv;
        tlv_temp += align(sizeof(Tlv) + length);
        tlv = (Tlv *)tlv_temp;
    }
}
