all: test_main
#all: out_send out_sc test_main
#out_send: temp_send.o
#	gcc -o out_send temp_send.o
#	rm -f temp_send.o
#temp_send.o: send.c sc.h
#	gcc -c send.c -o temp_send.o

#out_send: sc_send.cpp sc_class.cpp util.cpp
#	g++ sc_send.cpp sc_class.cpp util.cpp -o out_send
#	rm -f out_send.o

#out_sc: sc_server.cpp sc_class.cpp util.cpp
#	g++ sc_server.cpp sc_class.cpp util.cpp  -o out_sc -lrt
#	rm -f out_sc.o

test_main: test-main.cpp sc_class.cpp util.cpp
	g++ test-main.cpp sc_class.cpp util.cpp  -o test_main -lrt
	rm -f test_main.o

clean:
	rm -f test_main out_send out_server out_sc
