#include "sc.h"

void SC::fill_address_to_frame(uint8_t *ether_frame, char *str_dst_mac){
	char arr_mac[19]; // strtok can not use char *, so array char instead!!
	arr_mac[18] = '\0';

	strncpy(arr_mac, str_dst_mac, 18);
	set_mac(dst_mac, arr_mac);
	// Destination and Source MAC addresses
	printf("des mac address is ");
	output_mac(dst_mac);

	strncpy(arr_mac, program_mac_address, 18);
	set_mac(src_mac, arr_mac);
	printf("src mac address is ");
	output_mac(src_mac);

	// mac address
	memcpy (ether_frame, dst_mac, 6);
	memcpy (ether_frame + 6, src_mac, 6);
}

void SC::set_program_mac(char *mac_address){
    if (strlen(mac_address) != 17){
        sc_error("set program_mac_address error, format error!");
        return;
    }
    strncpy(program_mac_address, mac_address, 19);
    return;
}

// input format: "88:88:88:88:88:88"
void SC::add_listen_mac(char *listen_mac_address){
    string str_listen_mac_address;
    for (int i=0; i<strlen(listen_mac_address); i++){
        str_listen_mac_address += listen_mac_address[i];
    }
    vecListenMac.push_back(str_listen_mac_address);
}

void SC::sc_send(uint8_t *ether_frame, int frame_length){
    int sd, bytes;
    struct sockaddr_ll device;
    memset (&device, 0, sizeof (device));

    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[0]);
    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[1]);
    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[2]);
    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[3]);
    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[4]);
    //  printf("++++++SRC_MAC+++++++:%x\n", ether_frame[5]);

    // Find interface index from interface name and store index in
    // struct sockaddr_ll device, which will be used as an argument of sendto().
    if ((device.sll_ifindex = if_nametoindex (interface)) == 0) {
        sc_error ("if_nametoindex() failed to obtain interface index ");
        exit (EXIT_FAILURE);
    }
    //printf ("Index for interface %s is %i\n", interface, device.sll_ifindex);

     // Fill out sockaddr_ll.
    device.sll_family = AF_PACKET;
    memcpy (device.sll_addr, src_mac, 6);
    device.sll_halen = htons (6);

    // Submit request for a raw socket descriptor.
    if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_IP))) < 0) {//创建正真发送的socket
        sc_error ("socket() failed ");
        exit (EXIT_FAILURE);
    }
    // Send ethernet frame to socket.
    if ((bytes = sendto (sd, ether_frame, frame_length, 0, (struct sockaddr *) &device, sizeof (device))) <= 0) {
        sc_error ("sendto() failed");
        exit (EXIT_FAILURE);
    }
    printf ("send num=%d,read num=%d\n\n",frame_length,bytes);
    close (sd);
}

// 封装发送同步报文
// TODO: 单独send sync报文不需要封装吧..
void SC::send_sync_msg(char *data, int total, int fragment_number){
    Tlv *p = (Tlv *)(data+total);
    add_tlv(&p, 0, 0, '\0');
    total += sizeof(Tlv);

    // send
    uint8_t ether_frame[IP_MAXPACKET];

    uint8_t sync_src_mac[6];
    uint8_t sync_dst_mac[6];
    char arr_mac[19]; // strtok can not use char *, so array char instead!!
    arr_mac[18] = '\0';

    strncpy(arr_mac, program_mac_address, 18);
    set_mac(sync_src_mac, arr_mac);
    strncpy(arr_mac, sync_mac_address, 18);
    set_mac(sync_dst_mac, arr_mac);

    memcpy (ether_frame, sync_dst_mac, 6);
    memcpy (ether_frame + 6, sync_src_mac, 6);

    // protocol
    ether_frame[12] = ETH_P_DEAN / 256;
    ether_frame[13] = ETH_P_DEAN % 256;
    // control field
    Control c;
    memset(&c, 0, sizeof(Control));
    c.type = E_SYNC; // type is sync
    c.start = 1;
    c.end = 1;
    c.fragment_number = fragment_number; //分片号为0
    c.channel_id = 2;
    memcpy(ether_frame + 14, &c, sizeof(Control));
    memcpy (ether_frame + 22 , data, total);
    int frame_length = 6 + 6 + 2 + 8 + total;
    output_packet(ether_frame);
    sc_send(ether_frame, frame_length);

    return;
}

// index为vec_student上次send的位置.
void SC::syncStudentTbl(int index, int fragment_number)
{
    //把现有的vec_student中的所有学生信息组成报文
    char data[1024];
    int total = 0;
    for (int i=index; i<vec_student.size(); i++)
    {
        // 916怎么得到的呢？考虑到要再发送一个学生最多得 3*4+12+16+64+4=108，所以1024-108=916
        if (total >= 916){
            send_sync_msg(data, total, fragment_number);
            //未发送的继续同步
            syncStudentTbl(index, fragment_number+1);
            return;
        }
        studentInfo *pstudent = vec_student[i];
        //把信息放入tlvs
        // first tlv, number
        Tlv *p = (Tlv *)(void *)data;
        Tlv *tlv_start = p;
        int length = 0;

        length = strlen(pstudent->szStudent);
        add_tlv(&p, 1, length, pstudent->szStudent);
        total += align(sizeof(Tlv) + length);

        // second tlv, name
        length = strlen(pstudent->szStudentName);
        add_tlv(&p, 2, length, pstudent->szStudentName);
        total += align(sizeof(Tlv) + length);

        // third tlv, school
        length = strlen(pstudent->szStudentSchool);
        add_tlv(&p, 3, length, pstudent->szStudentSchool);
        total += align(sizeof(Tlv) + length);

        format_output_student(pstudent);

        index++;
    }
    // 如果所有的学生还不够填满一个分片[即1024],则直接发送，无需递归 :)
    send_sync_msg(data, total, fragment_number);
}

//周期函数的回调
void SC::check_msg_time()
{
    if (is_first){
        // 如果目前位置一条报文都没有，则直接返回.
        return;
    }
    // 同步报文, 30秒的周期
    sync_time += time_interval_seconds;
    if (sync_time > time_interval_sync_seconds){
        sync_time = 0;
        printf("time to sync msg now! the students inforation is below:\n");
        syncStudentTbl(0, 0);
    }
    //检测所有的mapMsgTime，查看时间是否超过了5s
    map<string, float>::iterator it;
    for (it = mapMsgTime.begin(); it != mapMsgTime.end(); it++)
    {
        float latest_time = it->second;
        string strKey = it->first;
        //如果报文已经到达完毕了，则不需要再检查了
        for (int i=0; i<vec_msg_done.size(); i++){
            if (vec_msg_done[i] == strKey){
                return;
            }
        }

        /*<!--简单测试读取现在所有的mapMsg*/
        // list<msgInfo*> node_list = mapMsg[strKey];
        // list<msgInfo*>::iterator list_it;
        // for (list_it = node_list.begin(); list_it != node_list.end(); list_it++){
        // 	msgInfo *p = *list_it;
        // 	cout << "fragment_number" << p->m_iMsgPartID << endl;
        // }
        /*简单测试读取现在所有的mapMsg--!>*/
        if (latest_time > time_interval_del_seconds){
            //删mapMsg中该strKey的所有内容
            list<msgInfo*> node_list = mapMsg[strKey];
            list<msgInfo*>::iterator it;
            for (it = node_list.begin(); it != node_list.end(); it++){
                msgInfo *p = *it;
                delete p;
            }
            mapMsg.erase(strKey);
        } else {
            it->second += time_interval_seconds;
            // TEST
            //printf("add it->second by %02f, now it is %02f\n", time_interval_seconds, it->second);
        }
    }
    return;
}

// 检测报文tlvs是否有效
// 1. 需要有结束报文
// 2. tlv总长度最大为1024
bool SC::test_tlvs_valid(Tlv *head){
    Tlv *p = head;
    char *tlv_temp = NULL;
    int length = 0;
    int tlv_total_len = 0;
    while (p->type != 0 && tlv_total_len <= 1024){
        if (p->type == 1 && p->len > 12){
            return false;
        } else if (p->type == 2 && p->len > 16){
            return false;
        } else if (p->type == 3 && p->len > 64){
            return false;
        }
        length = p->len;
        tlv_temp = (char *)p;
        tlv_temp += align(sizeof(Tlv) + length);
        p = (Tlv *)tlv_temp;
        tlv_total_len += align(sizeof(Tlv) + length);
    }
    if (tlv_total_len > 1024){
        return false;
    }
    return true;
}

//回复ACK或者RJT
// NOTE: 把src_mac变为dst_mac, dst_mac转为src_mac
void SC::send_back_packet(CONTROL_TYPE type){
    if (EXIT_SUCCESS != test_interface((char *)interface)){
        exit(0);
    }
    int frame_length = 6 + 6 + 2 + 8;;
    uint8_t ether_frame[IP_MAXPACKET];

    memcpy (ether_frame, src_mac, 6);
    memcpy (ether_frame + 6, dst_mac, 6);
    // protocol
    ether_frame[12] = ETH_P_DEAN / 256;
    ether_frame[13] = ETH_P_DEAN % 256;

    Control c;
    memset(&c, 0, sizeof(Control));
    c.start = 1;
    c.end = 1;
    c.fragment_number = 1;
    c.channel_id = 2; //TODO: 把channel_id设置为收到的channel_id

    if (E_ACK == type){
        sc_log("I will give back an ACK..\n");
        // control field
        c.type = E_ACK;
        memcpy(ether_frame + 14, &c, sizeof(Control));
    } else if (E_RJT == type){
        sc_log("I will give back a RJT..\n");
        // control field
        c.type = E_RJT;
        memcpy(ether_frame + 14, &c, sizeof(Control));
    }
    //先sleep一段时间,为了让send端有时间差，确保可以收到回复.
    usleep(50);
    sc_send(ether_frame,frame_length);
    return;
}

void SC::getInfoFromMsgPart(IN char *buf, OUT msgInfo *pstMsgInfo)
{
    string strMsgPart;
    for (int i=0; i < 2048; i++)
    {
        strMsgPart += buf[i];
    }

    // dst mac address
    string str_src_mac;
    char temp[18];
    snprintf(temp, 18, "%02x:%02x:%02x:%02x:%02x:%02x", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5]);
    str_src_mac += temp;

    (pstMsgInfo->srcMac).assign(str_src_mac);
    Control *control_field = (Control *)(buf + 14);

    string strDstMac_k;
    string strSrcMac_k;
    string strMsgChannelID_k;
    bool bIsMsgBegin = control_field->start;
    bool bIsMsgEnd = control_field->end;
    int fragment_number = control_field->fragment_number;
    int channel_id = control_field->channel_id;

    strDstMac_k.assign(strMsgPart, 0, 6);
    strSrcMac_k.assign(strMsgPart, 6, 6);
    pstMsgInfo->msg_type = control_field->type;
    pstMsgInfo->strTlvs.assign(strMsgPart.begin() + 22, strMsgPart.end());
    //分片号
    pstMsgInfo->iMsgPartID = fragment_number;
    //会话ID
    char str_channel_id[10];
    sprintf(str_channel_id, "%d", channel_id);
    strMsgChannelID_k = str_channel_id;

    pstMsgInfo->strKey = strDstMac_k + strSrcMac_k + strMsgChannelID_k;
    pstMsgInfo->iMsgBeginPart = bIsMsgBegin;
    pstMsgInfo->iMsgEndPart = bIsMsgEnd;
}

void SC::addStudent(studentInfo *pstudent)
{
	//NOTE 避免重复存储,查找有没有这个学生
	for (int i=0; i<vec_student.size(); i++){
		if (strcmp(vec_student[i]->szStudentName, pstudent->szStudentName) == 0){
			return;
		}
	}
	printf("add a student, name is %s, school is %s\n", pstudent->szStudentName, pstudent->szStudentSchool);
    vec_student.push_back(pstudent);
    return;
}

//从全局student map中delete
bool SC::deleteStudent(char *student_name){
    bool result = false;
    for (int i=0; i<vec_student.size(); i++){
        studentInfo *p = vec_student[i];
        if ((p != NULL) && (strncmp(student_name, p->szStudentName, strlen(p->szStudentName)) == 0)){
            //如果找到，则删除
            printf("delete a student, name is %s\n", p->szStudentName);
            delete p;
            result = true;
        }
    }
    return result;
}

//删某个strKey的所有msg
void SC::delete_msgs_by_key(string strKey){
    list<msgInfo*> node_list = mapMsg[strKey];
    list<msgInfo*>::iterator it;
    printf("delete all msgs of the key\n");
    int i=0;
    for (it = node_list.begin(); it != node_list.end(); it++){
        printf("delete %d\n", i++);
        msgInfo *p = *it;
        delete p; //释放节点
    }
    mapMsg.erase(strKey); //删除map[strKey]
}

//存储同步过来的学生
void SC::store_sync_student(string srcMac, studentInfo *pstudent){
    cout << "store a sync-got student with name of: " << pstudent->szStudentName << " it is from " << srcMac << endl;
    mapSyncStudent[srcMac].push_back(pstudent);
}

//把刚刚存储的同步学生打印
void SC::show_all_sync_students(string srcMac){
    list<studentInfo*> node_list = mapSyncStudent[srcMac];
    list<studentInfo*>::iterator it;
    cout << "Now show all students sync-got from " << srcMac << endl;
    for (it = node_list.begin(); it != node_list.end(); it++){
        format_output_student(*it);
    }
}

//处理某一个msg节点
bool SC::deal_msg_node(msgInfo *pmsgInfo, char last_node_student_name[12], char last_node_student_number[16]){
    uint8_t msg_type = pmsgInfo->msg_type;
    const int size = (pmsgInfo->strTlvs).size();
    char head[size];
    memset(head, 0, size);
    for (int i=0; i<size; i++){
        head[i] = (pmsgInfo->strTlvs)[i];
    }

    Tlv *p = (Tlv *)head;
    char *tlv_temp = NULL;
    int length = 0;
    int tlv_total_len = 0;
    studentInfo *pstudent = NULL;
    char student_number[12];
    char student_name[16];
    char student_school[64];
    memset(student_school, 0, 64);
    //NOTE 继承上一个msg可能的姓名||学号结尾
    strncpy(student_name, last_node_student_name, 12);
    strncpy(student_number, last_node_student_number, 16);
    while (p->type != 0 && tlv_total_len <= 1024){
        length = p->len;
        if (p->type == E_NUMBER){
            memcpy(student_number, p->data, length);
        } else if (p->type == E_NAME){
            memcpy(student_name, p->data, length);
        } else if (p->type == E_SCHOOL){
            memcpy(student_school, p->data, length);
            pstudent = new studentInfo();
            strncpy(pstudent->szStudent, student_number, 12);
            strncpy(pstudent->szStudentName, student_name, 16);
            strncpy(pstudent->szStudentSchool, student_school, 64);
            if (msg_type == E_ADD){
                addStudent(pstudent);
            } else if (msg_type == E_DEL){
                if (false == deleteStudent(pstudent->szStudentName)){
                    //处理失败
                    return false;
                }
            } else if (msg_type == E_SYNC){
                //同步报文，把对应的src_mac的所有传来的学生存储并且打印出来.
                store_sync_student(pmsgInfo->srcMac, pstudent);
            }
        }
        tlv_temp = (char *)p;
        tlv_temp += align(sizeof(Tlv) + length);
        p = (Tlv *)tlv_temp;
        tlv_total_len += align(sizeof(Tlv) + length);
    }
    //最后如果是学号和学生姓名结尾，则把他们传到下一个msg处理流程中
    if (student_name[0] != '\0'){
        strncpy(last_node_student_name, student_name, 12);
    }
    if (student_number[0] != '\0'){
        strncpy(last_node_student_number, student_number, 16);
    }
    return true;
}

//处理所有msgPart,当最后报文到来，且正确无误时执行
//成功则回复ACK
//失败则回复RJT
void SC::deal_all_msg(list<msgInfo*> &node_list, bool need_back){
    list<msgInfo*>::iterator it;
    char student_name[12];
    char student_number[16];
    memset(student_name, 0, 12);
    memset(student_number, 0, 16);
    for (it = node_list.begin(); it != node_list.end(); it++){
        msgInfo *p = *it;
        if (false == deal_msg_node(p, student_name, student_number)){
            //回复RJT
			if (need_back){
				send_back_packet(E_RJT);
			}
            return;
        }
    }
    //回复ACK
	//TODO 同步报文无需回复
	if (need_back){
		send_back_packet(E_ACK);
	}
	return;
}

void SC::putInfo2Map(IN msgInfo *pmsgInfo, IN string &strKey)
{
    //如果该分片号的报文已经存在，则直接throw该分片
    if (mapMsg.find(strKey) != mapMsg.end())
    {
        delete pmsgInfo;
        return;
    }
    else
    {
        list<msgInfo*>::iterator it;
        // ordered insert compared by partID
        for (it = mapMsg[strKey].begin(); it != mapMsg[strKey].end(); it++)
        {
            msgInfo *p = *it;
            if (p->iMsgPartID > pmsgInfo->iMsgPartID)
            {
                mapMsg[strKey].insert(it, pmsgInfo);
                break;
            }
        }
        if (it == mapMsg[strKey].end())
        {
            mapMsg[strKey].push_back(pmsgInfo);
        }
        //把最近报文的到来时间重置.
        mapMsgTime[strKey] = 0;
    }
    return;
}

//收到每个报文的处理函数
void SC::dealMsgPart(IN char *buf, IN bool need_back)
{
    string strMsgPart;
    for (int i=0; i < 2048; i++)
    {
        strMsgPart += buf[i];
    }

    msgInfo *pstMsgInfo = new msgInfo();
    memset(pstMsgInfo, sizeof(msgInfo), 0);

    /* 处理报文分片,提取关键数据 */
    getInfoFromMsgPart(buf, pstMsgInfo);

    //检测该分片是否为有效的, TODO 放在getInfoFromMsgPart中处理!
    if (!test_tlvs_valid((Tlv *)buf+22)){
        // 错误
        sc_error("the part of packet has error, so we throw it and delete all msgs related!\n");
        delete_msgs_by_key(pstMsgInfo->strKey);
        // 回复RJT报文
        //TODO 同步报文需要回复吗?
		if (need_back){
			send_back_packet(E_RJT);
		}
    }

    cout << "分片号: " << pstMsgInfo->iMsgPartID << endl;
    /* 放入HASHMAP中 */
    putInfo2Map(pstMsgInfo, pstMsgInfo->strKey);

	//如果来的是end分片.
	if (pstMsgInfo->iMsgEndPart == 1){
		string strKey = pstMsgInfo->strKey;
		//1.检查现有所有的分片号数目是否等于结束分片号
		int total_node_len = mapMsg[strKey].size();
		// 在putInfo2Map已经把end报文放进map中了.
		if (total_node_len == (pstMsgInfo->iMsgPartID)+1){
			//并且处理拼接整个报文
			list<msgInfo*> node_list = mapMsg[strKey];
			//处理所有的msgPart
			deal_all_msg(node_list, need_back);
			//把该strKey放入vec_msg_done中
			vec_msg_done.push_back(strKey);
		} else {
			//2.如果不等于，则说明报文不完整，删除所有的报文
			delete_msgs_by_key(strKey);
			return;
		}
	}
}

int SC::fill_demo_data(uint8_t *ether_frame, int fragment_number, int channel_id){
    int frame_length = 0;
    int datalen=0;
    int total = 0;
    uint8_t data[IP_MAXPACKET];
    memset(data, 0, IP_MAXPACKET);
    char *student_number = (char *)"U201212069";
    char *student_name = (char *)"laruence";
    char *student_school = (char *)"computer science";

    // first tlv, number
    Tlv *p = (Tlv *)(void *)data;
    Tlv *tlv_start = p;
    int length = 0;

    length = strlen(student_number);
    add_tlv(&p, 1, length, student_number);
    total += align(sizeof(Tlv) + length);

    // second tlv, name
    length = strlen(student_name);
    add_tlv(&p, 2, length, student_name);
    total += align(sizeof(Tlv) + length);

    // third tlv, school
    length = strlen(student_school);
    add_tlv(&p, 3, length, student_school);
    total += align(sizeof(Tlv) + length);

    // end tlv
    add_tlv(&p, 0, 0, '\0');
    total += sizeof(Tlv);

    // 发送的data，长度可以任意，但是抓包时看到最小数据长度为46，这是以太网协议规定以太网帧数据域部分最小为46字节，不足的自动补零处理
    datalen = total;
    memcpy(data, tlv_start, total);

    // Fill out ethernet frame header.
    frame_length = 6 + 6 + 2 + 8 + datalen;

    // protocol
    ether_frame[12] = ETH_P_DEAN / 256;
    ether_frame[13] = ETH_P_DEAN % 256;

    // control field
    Control c;
    memset(&c, 0, sizeof(Control));
    c.type = E_ADD;
    c.start = 1;
    c.end = 1;
    c.fragment_number = fragment_number; //分片号为0
    c.channel_id = channel_id;
    memcpy(ether_frame + 14, &c, sizeof(Control));
    memcpy (ether_frame + 22 , data, datalen);

    return frame_length;
}

void SC::changeMacAddr2Array(INOUT string &strMacAddr)
{
    /* 处理输入的MAC地址，可封成函数 */
    for (string::iterator it = strMacAddr.begin(); it != strMacAddr.end(); it++)
    {
        if ('-' == *it || ':' == *it)
        {
            strMacAddr.erase(it);
        }

        if (*it >= 'a' && *it <= 'f')
        {
            *it = *it - 32;
        }
    }
}
