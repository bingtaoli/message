#include "sc.h"

#define CLOCKID CLOCK_REALTIME
#define UINT unsigned int
#define CFG_NAME "CfgInfo.txt"
#define CFG_RES_NAME "CfgInfo_res.txt"

#define UCHAR uint8_t
#define UINT unsigned int
#define CHAR char

typedef struct tagPktInfo
{
	UINT uiTime;
	UINT uiPktLen;
	UCHAR aucPkt[2048];
	string type; // R 有RJT || ACK 两种type
	char filename[64];
} PktInfo_S;

inline int64_t get_sys_time(){
	struct timeval tv;
	gettimeofday (&tv , NULL);
	return tv.tv_sec * 1000 + (int64_t)(tv.tv_usec / 1000);
}

// 根据配置文件解析mac时使用的split函数
string sc_split(string str, string separator){
	int cutAt;
	string temp_mac;
	while((cutAt = str.find_first_of(separator)) != str.npos){
		if(cutAt > 0){
			temp_mac = str.substr(cutAt+1);
			break;
			//result.push_back(str.substr(0, cutAt));
		}
	}
	if (temp_mac.size()>0){
		//去空白符
		for (string::iterator it = temp_mac.begin(); it != temp_mac.end(); it++){
			if (' ' == *it){
				temp_mac.erase(it);
			}
		}
		return temp_mac;
	}
	return NULL;
}

void CheckCFG(string &str_dst_mac)
{
	std::ifstream cfginfo(CFG_NAME);
	std::ifstream cfginfores(CFG_RES_NAME);
	std::string cfgstr;
	std::string resstr;
	bool cfgcheck = true;
	while (!cfginfores.eof())
	{
		if (cfginfo.eof())
		{
			cfgcheck = false;
			printf("CFG Check Failed.\n");
		}
		getline(cfginfo, cfgstr);
		getline(cfginfores, resstr);
		if (cfgstr != resstr)
		{
			cfgcheck = false;
			printf("CFG Check Failed.\n");
		}
		if (resstr.find("local") != std::string::npos){
			str_dst_mac = sc_split(resstr, ":");
		}
	}
	if (cfginfo.eof())
	{
		printf("CFG Check Successed.\n");
	}
	else
	{
		printf("CFG Check Failed.\n");
	}
	cfginfo.close();
	cfginfores.close();
}

UINT GetPacket(CHAR * pcPath, UCHAR * pucBuffer, UINT uiMaxLen)
{
	std::ifstream fin(pcPath, std::ios::binary);

	if (fin.bad()){
		printf("Failed to open file.\n");
		return 0;
	}
	UINT uiLength = 0;
	memset(pucBuffer, 0, uiMaxLen * sizeof(char));
	char temp_pcap_buf[40];
	int temp_head_length = 0;
	while (!fin.eof())
	{
		if (temp_head_length < 40){
			fin.read(temp_pcap_buf+temp_head_length, sizeof(char));
			temp_head_length++;
			continue;
		}
		fin.read((CHAR *)pucBuffer + (uiLength), sizeof(UCHAR));
		uiLength++;
		if (uiLength >= uiMaxLen)
		{
			break;
		}
	}
	/* pucBuffer[15] = (pucBuffer[15] & 0x01)*128
	+  ((pucBuffer[15] >> 1) & 0x01)*64
	+  ((pucBuffer[15] >> 2) & 0x01)*64
	+  ((pucBuffer[15] >> 3) & 0x01)*32
	+  ((pucBuffer[15] >> 4) & 0x01)*8
	+  ((pucBuffer[15] >> 5) & 0x01)*4
	+  ((pucBuffer[15] >> 6) & 0x01)*2
	+  ((pucBuffer[15] >> 7) & 0x01);
	printf("*************************************\n");  */
	// for (int i = 0; i < uiLength; i++)
	// {
	// 	printf("%02x", pucBuffer[i]);
	// }
	//
	// printf("*************************************\n");
	fin.close();
	return uiLength - 1;
}

void ReadTestCFG(char *TestCfgName, PktInfo_S *Send, int &SendCnt, PktInfo_S *Recv, int &RecvCnt, int &SyncTime)
{
	std::ifstream fin(TestCfgName);
	SendCnt = 0;
	RecvCnt = 0;
	UINT nowtime = 0;

	while (!fin.eof())
	{
		char Opt;
		fin >> Opt;
		if (fin.eof()){
			break;
		}
		cout << Opt << endl;
		if ('S' == Opt)
		{
			UINT time;
			char PktName[64];
			fin >> time;
			fin >> PktName;
			nowtime = time;
			Send[SendCnt].uiTime = time;
			strcpy(Send[SendCnt].filename, PktName);
			Send[SendCnt].uiPktLen = GetPacket(PktName, Send[SendCnt].aucPkt, 2048);
			SendCnt++;
		}
		else if ('R' == Opt)
		{
			UINT time;
			string type;
			fin >> time;
			Recv[RecvCnt].uiTime = time;
			fin >> type;
			Recv[RecvCnt].type = type;
			RecvCnt++;
		}
		else if ('Y' == Opt)
		{
			UINT time;
			fin >> time;
			SyncTime = time;
			cout << "sync time: " << SyncTime << endl;
		}
	}
	printf("Read Test CFG Successed.\n");
	return;
}

int SC::test_main(){
	if (EXIT_SUCCESS != test_interface(interface)){
		return EXIT_FAILURE;
	}
	string str_dst_mac;
	/* 检查配置输出,基本的配置文件测试，即local_mac能否还原 */
	CheckCFG(str_dst_mac);
	if (str_dst_mac.size() <= 0){
		perror("config file error, local_mac error");
		exit(1);
	}
	cout << str_dst_mac << endl;

	PktInfo_S Send[16], Recv[16];
	int SendCnt=0, RecvCnt=0, SyncTime=0;
	char *TestCfgName = (char *)"TestCfg.txt";
	/* 读取测试配置文件 */
	ReadTestCFG(TestCfgName, Send, SendCnt, Recv, RecvCnt, SyncTime);
	int SendIndex = 0, RecvIndex = 0;
	int64_t start_time = get_sys_time();
	int  SOCKET_SRC;
	char buf[2048];
	int n_rd;

	if ((SOCKET_SRC = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0)
	{
		printf("create socket error.\n");
		exit(0);
	}

	fd_set fds;
	FD_SET(SOCKET_SRC, &fds);
	struct timeval timeout={0, 0}; //非阻塞就置0

	uint8_t temp_local_mac[6];
	temp_local_mac[0] = 0X01;
	temp_local_mac[1] = 0X23;
	temp_local_mac[2] = 0X21;
	temp_local_mac[3] = 0X14;
	temp_local_mac[4] = 0XAA;
	temp_local_mac[5] = 0XBB;

	while ((SendIndex < SendCnt) || (RecvIndex < RecvCnt) || SyncTime > 0)
	{
		int64_t now_time = get_sys_time();
		if ( (SendIndex < SendCnt) && (now_time - start_time > Send[SendIndex].uiTime * 1000))
		{
			sc_log(">>>send %d, from file %s\n", SendIndex+1, Send[SendIndex].filename);
			//output_packet(Send[SendIndex].aucPkt);
			sc_send(Send[SendIndex].aucPkt, Send[SendIndex].uiPktLen);
			SendIndex++;
		}
		switch(select(SOCKET_SRC+1, &fds, &fds, NULL, &timeout))
		{
			case -1: {
				exit(-1);
				break;
			}
			case 0:{
				continue;
			}
			default:{
				if(FD_ISSET(SOCKET_SRC, &fds)){
					n_rd = recvfrom(SOCKET_SRC, buf, 2048, MSG_DONTWAIT, NULL, NULL);
				}
			}
		}
		// if (is_our_protocol(buf)){
		// 	printf("\nrecv_dst:");
		// 	output_mac((uint8_t*)(buf+6));
		// 	printf("\nrecv_src:");
		// 	output_mac((uint8_t*)buf);
		// 	printf("\ninput_send_dst");
		// 	output_mac(temp_local_mac);
		// }
		if ( n_rd == -1 || !is_our_protocol(buf) || memcmp(buf + 6, temp_local_mac, 6) != 0){
			if ((RecvIndex < RecvCnt) && now_time - start_time > (Recv[RecvIndex].uiTime + 1) * 1000)
			{
				printf("Recv Pkt%3d: Packet NOT Receive.\n\n", RecvIndex);
				RecvIndex++; //当前用例失败
			}
			if ((SyncTime > 0) && (now_time - start_time > SyncTime * 1000))
			{
				printf("Sync NOT Receive in %ds.\n", SyncTime);
				SyncTime = 0; // 同步报文失败
			}
			continue;
		}

		Control *control_field = (Control *)(buf + 14);
		if ( (RecvIndex < RecvCnt) && (control_field->type == E_ACK) && (now_time - start_time <= (Recv[RecvIndex].uiTime + 1) * 1000)){
			if (Recv[RecvIndex].type == "ACK"){
				sc_log("<<<%3d back msg type: ACK!\n", RecvIndex);
			} else {
				sc_log("<<<%3d needs a RJT, but we got an ACK!\n", RecvIndex);
			}
			output_packet((uint8_t *)buf);
			RecvIndex++;
		} else if ((RecvIndex < RecvCnt) && (control_field->type == E_RJT) && (now_time - start_time <= (Recv[RecvIndex].uiTime + 1) * 1000)){
			if (Recv[RecvIndex].type == "RJT"){
				sc_log("<<<%3d back msg type: RJT!\n", RecvIndex);
			} else {
				sc_log("<<<%3d needs an ACK, but we got a RJT!\n", RecvIndex);
			}
			output_packet((uint8_t *)buf);
			RecvIndex++;
		} else if (control_field->type == E_SYNC && (SyncTime > 0) && (now_time - start_time < SyncTime * 1000)){
			//同步报文处理，只是在deal_all_msg处有区别
			dealMsgPart(buf, false);
			//只有在end报文到来时才打印所有学生
			if (control_field->end == 1){
				string str_src_mac;
				char temp[18];
				snprintf(temp, 18, "%02x:%02x:%02x:%02x:%02x:%02x", src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5]);
				str_src_mac += temp;
				show_all_sync_students(str_src_mac);
				SyncTime = 0; //同步报文OK!
			}
		}
	}
}

int main()
{
	SC *sc = new SC();
	sc->test_main();
	return 0;
}
