#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy()
#include <netdb.h>            // struct addrinfo, uint8_t, uint16_t, uint32_t
#include <sys/socket.h>       // needed for socket()
#include <netinet/in.h>       // IPPROTO_ICMP, INET_ADDRSTRLEN
#include <netinet/ip.h>       // struct ip and IP_MAXPACKET (which is 65535)
#include <netinet/ip_icmp.h>  // struct icmp, ICMP_ECHO
#include <arpa/inet.h>        // inet_pton() and inet_ntop()
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <linux/if_ether.h>   // ETH_P_IP = 0x0800, ETH_P_IPV6 = 0x86DD
#include <linux/if_packet.h>  // struct sockaddr_ll (see man 7 packet)
#include <net/ethernet.h>
#include <errno.h>            // errno, perror()
#include <stdarg.h>           // log function
#include <time.h>
#include <ctime>
#include <sys/time.h>
#include <signal.h>           //signal()
#include "util.h"

using namespace std;

#define DEBUG 1

#define IN
#define OUT
#define INOUT

#define ETH_P_DEAN 0x1122 //自定义的种子协议

static char *interface = (char *)"eth0";
const char sync_mac_address[19] = "01:80:c2:dd:fe:ff"; //同步报文

#define true (ushort)(1)
#define false (ushort)(0)
#define bool ushort

// 控制域类型
enum CONTROL_TYPE
{
    E_ADD=1, E_DEL=2, E_ACK=3, E_RJT=4, E_SYNC=5,
};

enum TLV_TYPE {
    E_END = 0,
    E_NUMBER = 1,
    E_NAME = 2,
    E_SCHOOL = 3,
};

typedef struct tlv {
	uint8_t type; // 学号，姓名，学院，结束
	uint8_t len;
	char data[0]; //soft array, do not use char *data.
} Tlv;

typedef struct control{
    uint8_t type;
    unsigned start : 1;
    unsigned end : 1;
    unsigned fragment_number : 22;
    uint32_t channel_id;
} Control;

typedef struct tag_studentInfo
{
    char szStudent[12];
    char szStudentName[16];
    char szStudentSchool[64];
}studentInfo;

typedef struct tag_MsgInfo
{
    uint8_t msg_type;
    int iMsgPartID;
    int iMsgBeginPart;
    int iMsgEndPart;
    string strKey;
    string strTlvs;
    string srcMac;  //同步报文时使用
}msgInfo;

//　这些工具类函数在util.cpp中实现
uint8_t align(uint8_t size);
// hex to dec
uint8_t wtoi(char *hexa);
/*
@str_mac: eg 88:88:88:88:88:88
@dst_mac: uint8_t dst_mac[6]
*/
void set_mac(OUT uint8_t dst_mac[6], IN char *str_mac);
void output_mac(uint8_t *mac);
void format_output_student(studentInfo *pstudent);
//检测网卡是否有效
int test_interface(char *interface);
void add_tlv(INOUT Tlv **buf_pointer, IN const uint8_t type, IN const uint8_t len, IN const char *data);
//是否是我们需要的协议
bool is_our_protocol(char *buf);
//发送之前检测数据是否正确赋给buf, 仅仅测试使用!
void output_packet(uint8_t *ether_frame);

#define CLOCKID CLOCK_REALTIME

const float time_interval_seconds = 0.5;
const float time_interval_sync_seconds = 5.0;
const float time_interval_del_seconds = 30.0; // 30s还未有结束报文来则删除所有的msg
extern char *interface; //回复ACK或着RJT的目的网卡,所有实例全部通用，所以使用全局而不作为类变量

class SC {
private:
    // NOTE: 如果使用static,则会提示所有函数中对这些变量的引用为未定义的引用!
    map<string, list<msgInfo*> > mapMsg;
    map<string, float> mapMsgTime; //记录报文有多久未到达了, 一开始初始化为0, 开0.1秒定时器更新,来了新报文则重设为0
    vector<string> vec_msg_done; //记录哪些报文已经全部完成了
    vector<studentInfo*> vec_student;
    map<string, list<studentInfo*> > mapSyncStudent;

    bool is_first; //是否是第一个报文，如果是则起0.1秒的定时器.
    float sync_time; // 判断是否要同步报文了，大于5则同步，同步之后重置为0
    char program_mac_address[19]; //程序mac地址
    vector<string> vecListenMac; // 程序监听mac
    uint8_t src_mac[6]; //收到的报文src_mac
    uint8_t dst_mac[6]; //收到的报文dst_mac
    
public:
    SC() {
        sync_time = 0;
        is_first = true;
        strncpy(program_mac_address, (char *)"88:88:88:88:88:88", 19);
        add_listen_mac((char *)"88:88:88:88:88:88");
    }

    void set_program_mac(char *mac_address);
    // input format: "88:88:88:88:88:88"
    void add_listen_mac(char *listen_mac_address);
    void sc_send(uint8_t *ether_frame, int frame_length);
    // 封装发送同步报文
    // TODO: 单独send sync报文不需要封装吧..
    void send_sync_msg(char *data, int total, int fragment_number);
    // index为vec_student上次send的位置.
    void syncStudentTbl(int index, int fragment_number);
    //周期函数的回调
    void check_msg_time();
    // 检测报文tlvs是否有效
    // 1. 需要有结束报文
    // 2. tlv总长度最大为1024
    bool test_tlvs_valid(Tlv *head);
    //回复ACK或者RJT
    // NOTE: 把src_mac变为dst_mac, dst_mac转为src_mac
    void send_back_packet(CONTROL_TYPE type);
    void getInfoFromMsgPart(IN char *buf, OUT msgInfo *pstMsgInfo);
    void addStudent(studentInfo *pstudent);
    //从全局student map中delete
    bool deleteStudent(char *student_name);
    //删某个strKey的所有msg
    void delete_msgs_by_key(string strKey);
    //处理某一个msg节点
    bool deal_msg_node(msgInfo *pmsgInfo, char last_node_student_name[12], char last_node_student_number[16]);
    //处理所有msgPart,当最后报文到来，且正确无误时执行
    //成功则回复ACK
    //失败则回复RJT
    void deal_all_msg(list<msgInfo*> &node_list, bool need_back);
    void putInfo2Map(IN msgInfo *pmsgInfo, IN string &strKey);
    //收到每个报文的处理函数
    void dealMsgPart(IN char *buf, IN bool need_back = true);
    void changeMacAddr2Array(INOUT string &strMacAddr);
    void store_sync_student(string srcMac, studentInfo *pstudent);
    void show_all_sync_students(string srcMac);
    void fill_address_to_frame(uint8_t *ether_frame, char *str_dst_mac);
    //此函数取代send.c作为发送端
    int send_client();
    int fill_demo_data(uint8_t *ether_frame, int fragment_number, int channel_id);
    //NOTE 测试主函数
    int test_main();
    void doloop();
};
